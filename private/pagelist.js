var PAGE_LIST = [
	[
		"PAGE",
		"Home",
		"home",
		"May 25, 2017",
		"<h1>Welcome to <strong>BeePages</strong>!</h1><p><br></p><h3>It's great to ",
		"",
		""
	],
	[
		"POST",
		"My%20First%20Post",
		"first-post",
		"July 15, 2017",
		"<p>This is my very first post using Beezy. So far it is pretty great, actua",
		"",
		""
	],
	[
		"PAGE",
		"About Us",
		"about",
		"May 31, 2017",
		"<p><span class=\"ql-size-huge\">About BeePages</span></p><p><img src=\"/upload",
		"",
		""
	],
	[
		"POST",
		"Devlog 2",
		"devlog-2",
		"July 12, 2017",
		"<p>This is going really great ;)</p>",
		"",
		"h"
	],
	[
		"POST",
		"Devlog 3",
		"devlog-3",
		"July 13, 2017",
		"<p>BeePages v1 is near completion. The planned date for the first release i",
		"",
		"d"
	]
];
var PAGE_LIST_LANDING = 0; var module = module || {}; module.exports = {LIST: PAGE_LIST, LANDING: PAGE_LIST_LANDING};