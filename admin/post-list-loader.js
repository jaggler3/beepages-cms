function showPosts()
{
	updatePageList(function()
	{
		displayPosts();
	});
}

function displayPosts()
{
	let listContainer = document.getElementById("post-list");
	listContainer.innerHTML = "";
	for(let i = 0; i < PAGE_LIST.length; i++)
	{
		if(PAGE_LIST[i][0] == "PAGE") { continue; }
		let entry = PAGE_LIST[i];
		let pageState = (entry[6] == "d" ? "draft" : (entry[6] == "h" ? "hidden" : ""));
		getFileContents('admin/views/entry?token=' + window.bee.sessionToken, function(data)
		{
			let entryEleP = document.createElement('div');
			entryEleP.innerHTML = data;
			let entryElement = entryEleP.firstChild;
			if(pageState != "")
			{
				entryElement.className += " entry-" + pageState;
			}

			//update entry data
			//....
			let cl = entryElement.childNodes;
			for(let j = 0; j < cl.length; j++)
			{
				if(cl[j].className == "entry-title")
				{
					if(pageState == "draft")
					{
						cl[j].innerHTML = '<i class="fa fa-pencil mr" aria-hidden="true" title="Draft"></i>';
					} else if(pageState == "hidden")
					{
						cl[j].innerHTML = '<i class="fa fa-file mr" aria-hidden="true" title="Archived"></i>';
					} else
					{
						cl[j].innerHTML = "";
					}
					cl[j].innerHTML += decodeURIComponent(entry[1]);
				} else if(cl[j].className == "entry-link")
				{
					cl[j].innerHTML = '/posts/' + entry[2];
				} else if(cl[j].className == "entry-date")
				{
					cl[j].innerHTML = 'Last edited: ' + entry[3];
				} else if(cl[j].className == 'entry-edit')
				{
					cl[j].setAttribute('onclick', 'editPage("' + entry[2] + '")');
					if(pageState != "") { cl[j].className += " light"; }
				} else if(cl[j].className == 'entry-rename')
				{
					cl[j].setAttribute('onclick', 'showRenameDialog(' + i + ')');
					if(pageState != "") { cl[j].className += " light"; }
				} else if(cl[j].className == 'entry-delete')
				{
					cl[j].setAttribute('onclick', 'showDeleteDialog("' + entry[1] + '")');
					if(pageState != "") { cl[j].className += " light"; }
				}
			}

			listContainer.appendChild(entryElement);
		});
	}
}


function editPage(pageURL)
{
	selectView(undefined, 'editor', pageURL);
}

function openCategoriesDialog()
{

}
