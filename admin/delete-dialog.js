var _dn = undefined;
var _di = undefined;
function showDeleteDialog(name)
{
	_dialogOpen();
	_dn = name;
	_di = -1;
	for(let i = 0; i < PAGE_LIST.length; i++)
	{
		if(PAGE_LIST[i][1] == name)
		{
			_di = i;
			break;
		}
	}
	if(_di == -1)
	{
		console.log("ERROR: item not found");
	}
	//TODO: cache dialog data
	getFileContents('admin/dialogs/delete?token=' + window.bee.sessionToken, function(data){
		let dd = document.createElement('div');
		dd.innerHTML = data;
		let dialog = dd.firstChild;

		let children = dialog.childNodes;
		for(let i = 0; i < children.length; i++)
		{
			let child = children[i];
			if(child.id == "delete-name")
			{
				child.innerHTML = '[' + name + ']';
			} else if(child.id == "delete-dialog-accept")
			{
				child.setAttribute("onclick", "acceptDeleteDialog('" + name + "');");
			} else if(child.id == "delete-dialog-cancel")
			{
				child.setAttribute("onclick", "cancelDeleteDialog();");
			}
		}

		document.body.appendChild(dialog);
	});
}

function acceptDeleteDialog(_name)
{
	let xhr = new XMLHttpRequest();
	xhr.open("POST",  window.location.origin + '/delete', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		content:
		{
			name: encodeURIComponent(_name)
		}
	}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function () {
		updatePageList(function(){
			cancelDeleteDialog();
			_dialogClose();
			(window.bee.currentView == "posts" ? showPosts : showPages)();
		});
	};
}

function cancelDeleteDialog()
{
	let dialog = document.getElementById("delete-dialog");
	document.body.removeChild(dialog);
	_dialogClose();
}
