
var _rdi = undefined;
function showRenameDialog(index)
{
	_dialogOpen();
	_rdi = index;
	//TODO: cache dialog data
	getFileContents('admin/dialogs/rename?token=' + window.bee.sessionToken, function(data)
	{
		let dd = document.createElement('div');
		dd.innerHTML = data;
		let dialog = dd.firstChild;

		let children = dialog.childNodes;
		for(let i = 0; i < children.length; i++)
		{
			let child = children[i];
			if(child.id == "rename-name")
			{
				child.value = decodeURIComponent(PAGE_LIST[index][1]);
			} else if(child.id == "rename-url")
			{
				child.value = PAGE_LIST[index][2];
			} else if(child.id == "rename-dialog-accept")
			{
				child.setAttribute("onclick", "acceptRenameDialog();");
			} else if(child.id == "rename-dialog-cancel")
			{
				child.setAttribute("onclick", "cancelRenameDialog();");
			} else if(child.id == "url-prev")
			{
				child.innerHTML += PAGE_LIST[index][0] == "POST" ? "posts/" : "";
			}
		}

		document.body.appendChild(dialog);
	});
}

function acceptRenameDialog()
{
	let xhr = new XMLHttpRequest();
	xhr.open("POST", window.location.origin + '/rename', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		content:
		{
			type: PAGE_LIST[_rdi][0],
			previous: [PAGE_LIST[_rdi][1], PAGE_LIST[_rdi][2]],
			new: [encodeURIComponent(document.getElementById("rename-name").value), document.getElementById("rename-url").value],
		}
	}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function ()
	{
		updatePageList(function()
		{
			window.location.href = window.location.origin + "/admin/selected/" + window.bee.currentView + "?token=" + window.bee.sessionToken;
		});
	};
}

function cancelRenameDialog()
{
	let dialog = document.getElementById("rename-dialog");
	document.body.removeChild(dialog);
	_dialogClose();
}
