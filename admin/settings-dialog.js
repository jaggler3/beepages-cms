
var _osvs;
function showSettingsDialog()
{
	_dialogOpen();

	//TODO: cache dialog data
	getFileContents('admin/dialogs/settings?token=' + window.bee.sessionToken, function(data)
    {
        loadSettings(function(settingsData)
        {
			_osvs = settingsData;
            let dd = document.createElement('div');
            dd.innerHTML = data;
            let dialog = dd.firstChild;

            let children = dialog.childNodes;

            for(let i = 0; i < children.length; i++)
            {
                let child = children[i];
                if(child.id == "settings-dialog-accept")
                {
                    child.setAttribute("onclick", "acceptSettingsDialog()");
                } else if(child.id == "settings-dialog-cancel")
                {
                    child.setAttribute("onclick", "cancelSettingsDialog()");
                }
            }

			document.body.appendChild(dialog);
            document.getElementById("settings-sitename").value = settingsData.data["SITENAME"];
        });
	});
}

function loadSettings(cont)
{
    let xhr = new XMLHttpRequest();

	let dts = [
		["token", window.bee.sessionToken],
	];

	xhr.onreadystatechange = function ()
	{
		if(xhr.readyState == XMLHttpRequest.DONE)
		{
			if(xhr.status == 200)
			{
				cont(JSON.parse(xhr.responseText));
			}
			else
			{
				alert("Could not load settings.");
			}
		}
	};

	xhr.open("GET", window.location.origin + '/getsettings?' + formQuery(dts), true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
	xhr.send(null);
}

function acceptSettingsDialog()
{
	let xhr = new XMLHttpRequest();
	xhr.open("POST", window.location.origin + '/setsettings', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		content:
		{
            changes: []
		}
	}

	let snv = document.getElementById("settings-sitename").value;
	if(_osvs.data["SITENAME"] != snv) { dts.content.changes.push({key: "SITENAME", value:snv})}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function ()
    {
		updatePageList(function()
        {
			window.location.href = window.location.origin + "/admin/selected/" + window.bee.currentView + "?token=" + window.bee.sessionToken;
		});
	};
}

function cancelSettingsDialog()
{
	let dialog = document.getElementById("settings-dialog");
	document.body.removeChild(dialog);
	_dialogClose();
}
