
function showCreateDialog(_type)
{
	_dialogOpen();
	//TODO: cache dialog data
	getFileContents('admin/dialogs/newitem?token=' + window.bee.sessionToken, function(data){
		let dd = document.createElement('div');
		dd.innerHTML = data;
		let dialog = dd.firstChild;

		let children = dialog.childNodes;
		for(let i = 0; i < children.length; i++)
		{
			let child = children[i];
			if(child.id == "new-dialog-title")
			{
				let ndt = "New " + (_type == "PAGE" ? "Page" : (_type == "POST" ? "Post" : ""));
				child.innerHTML = ndt;
			} else if(child.id == "url-prev")
			{
				child.innerHTML += _type == "POST" ? "posts/" : "";
			} else if(child.id == "new-dialog-accept")
			{
				child.setAttribute("onclick", "acceptCreateDialog('" + _type + "');");
			} else if(child.id == "new-dialog-cancel")
			{
				child.setAttribute("onclick", "cancelCreateDialog();");
			}
		}

		document.body.appendChild(dialog);
	});
}

function acceptCreateDialog(_type)
{
	let xhr = new XMLHttpRequest();
	xhr.open("POST",  window.location.origin + '/create', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		content:
		{
			type: _type,
			new: [encodeURIComponent(document.getElementById("new-name").value), document.getElementById("new-url").value],
		}
	}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function () {
		//open editor
		let su = document.getElementById("new-url").value;
		updatePageList(function() {
			cancelCreateDialog();
			editPage(su);
		});
		_dialogClose();
	};
}

function cancelCreateDialog()
{
	let dialog = document.getElementById("new-dialog");
	document.body.removeChild(dialog);
	_dialogClose();
}
