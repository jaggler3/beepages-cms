var themeLocation = "https://serene-petrified-forest-51185.herokuapp.com";

function showThemeSelect()
{
	showThemePage("{{page}}" == "null" ? 0 : Number("{{page}}"));

	//when changing theme pages, go to the url to refresh the page
	//e.g. mysite.com/admin/selected/themes/[x]
}

function showThemePage(page)
{
	let example = document.getElementsByClassName("theme-item")[0];
	let themeList = document.getElementById("theme-list-container");
	
	getFileContents(themeLocation + '/themes?page=' + page, function(data)
	{
		let themeData = JSON.parse(data);
		//set example's data here...
		let fcn = example.childNodes;
		let ni = 0;
		for(let i = 0; i < fcn.length; i++)
		{
			example.style.backgroundImage = "url(" + themeLocation + "/files/" + themeData.previews[0];
			if(fcn[i].className == "theme-item-name")
			{
				fcn[i].innerHTML = themeData.themes[0][0];
			} else if(fcn[i].className == "theme-item-info")
			{
				fcn[i].innerHTML = decodeURI(themeData.themes[0][1]);
			} else if(fcn[i].className == "theme-item-install")
			{
				fcn[i].setAttribute("onclick", "installTheme('" + themeData.themes[0][0] + "')");
			}
		}

		//themes[i >= 1]
		for(let i = 1; i < themeData.themes.length; i++)
		{
			let exampleRt = example.cloneNode(true);
			exampleRt.style.backgroundImage = "url(" + themeLocation + "/" + themeData.previews[i];

			let cn = exampleRt.childNodes;
			for(let j = 0; j < cn.length; j++)
			{
				if(cn[i].className == "theme-item-name")
				{
					cn[i].innerHTML = themeData.themes[0][0];
				} else if(cn[i].className == "theme-item-info")
				{
					cn[i].innerHTML = themeData.themes[0][1];
				}
			}

			themeList.appendChild(exampleRt);
		}
		
	}, false);
}