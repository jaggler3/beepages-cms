var _new = false;

window.onload = function()
{
	getFileContents('admin/login/new', function(data)
	{
		if(data.trim() == "true")
		{
			_new = true;
			ele("login-submit").innerHTML = "Create admin";
			let info = ele("error");
			info.innerHTML = "Welcome to your site!";
			info.style.display = "block";
		}
	});
}

function getFileContents(path, callback, local = true)
{
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			callback(this.responseText);
		}
	};
	xhttp.open("GET", (local ? window.location.origin + "/" : "") + path, true);
	xhttp.send();
}

function submit()
{
	if(_new)
	{
		let xhr = new XMLHttpRequest();
		xhr.open("POST", window.location.origin + '/admin/login/create', true);
		xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

		let dts = {
			username: ele("login-username").value,
			password: ele("login-password").value
		}

		xhr.send(JSON.stringify(dts));

		xhr.onloadend = function () {
			tryLogin();
		};
	} else
	{
		tryLogin();
	}
}

function tryLogin()
{
	let xhr2 = new XMLHttpRequest();

	xhr2.onreadystatechange = function()
	{
		if (xhr2.readyState == XMLHttpRequest.DONE)
		{
			if (xhr2.status == 200)
			{
				let result = xhr2.responseText;
				if(result != "unsuccessful")
				{
					window.location.href = window.location.origin + "/admin?token=" + result;
				} else
				{
					let info = ele("error");
					info.innerHTML = "Incorrect username and password";
					info.style.display = "block";
				}
			}
			else
			{
				alert("Could not access server.");
			}
		}
	};

	console.log(window.location.origin + '/admin/login/try?username='+ele("login-username").value+"&password="+ele("login-password").value);
	xhr2.open("GET", window.location.origin + '/admin/login/try?username='+ele("login-username").value+"&password="+ele("login-password").value, true);
	xhr2.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
	xhr2.send();
}

function ele(i)
{
	return document.getElementById(i);
}
