function showAnalytics()
{
	loadAnalyticsData(function(data)
	{
		let topPageCounter = document.getElementById("acs-top-today");
		let topWeekCounter = document.getElementById("acs-top-week");
		let topAllCounter = document.getElementById("acs-top-all");

		let pv_names = document.getElementById("pageview-names");
		let pv_data = document.getElementById("pageview-data");

		topPageCounter.innerHTML = data.today_views;
		topWeekCounter.innerHTML = data.week_views;
		topAllCounter.innerHTML = data.all_views;

		pv_names.innerHTML = data.today_pages.join("\n");
		pv_data.innerHTML = data.today_data.join("\n");

		let ctx = document.getElementById("analytics-graph");
		let dates = data.week_dates;

		var myChart = new Chart(ctx,
		{
			type: 'bar',
			data: {
				labels: dates,
				datasets: [{
					label: '# of Views',
					data: data.week_data,
					backgroundColor: '#0095a8',
					borderColor: 'rgb(0, 0, 0)',
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});

	});
}

function loadAnalyticsData(callback)
{
	getFileContents('analytics_data?token=' + window.bee.sessionToken, function(data) {
		callback((function (){ let _rv = undefined; eval("_rv = " + data); return _rv; })());
	});
}
