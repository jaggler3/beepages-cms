
var _media_cb = undefined;
function showMediaDialog(callback)
{
	_media_cb = callback;
	_dialogOpen();

	//TODO: cache dialog data
	getFileContents('admin/dialogs/media?token=' + window.bee.sessionToken, function(data)
	{
		loadMedia(function(mediaData)
		{
			let dd = document.createElement('div');
			dd.innerHTML = data;
			let dialog = dd.firstChild;
			document.body.appendChild(dialog);

			var tmpEntry = document.getElementById("media-tmp-item");
			tmpEntry.style.display = "none";

			let entryList = document.getElementById("media-dialog-list");
			for(let i = 0; i < mediaData.length; i++)
			{
				let newEntry = tmpEntry.cloneNode(true);
				newEntry.style.display = "block";
				newEntry.className += " new";
				newEntry.setAttribute("onclick", "selectMedia('" + mediaData[i] + "')")
				for(let j = 0; j < newEntry.childNodes.length; j++)
				{
					if((newEntry.childNodes[j].className || "").indexOf("media-dialog-item-name") != -1)
					{
						newEntry.childNodes[j].innerHTML = mediaData[i];
					}
				}
				entryList.appendChild(newEntry);
			}

			document.getElementById("media-dialog-upload").setAttribute("onclick", "showMediaPrompt()");
			document.getElementById("media-dialog-cancel").setAttribute("onclick", "cancelMediaDialog()");
		});
	});
}

function selectMedia(name)
{
	_media_cb("/uploads/" + name);
	cancelMediaDialog();
}

function showMediaPrompt()
{
	document.getElementById('file-input').click();
}

function uploadMedia()
{
	document.getElementById("upload_form").submit();
	selectMedia(document.getElementById("upload_form").value);
}

function loadMedia(cont)
{
	let xhr = new XMLHttpRequest();

	let dts = [
		["token", window.bee.sessionToken],
	];

	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == XMLHttpRequest.DONE)
		{
			if (xhr.status == 200)
			{
				cont(JSON.parse(xhr.responseText));
			}
			else
			{
				alert("Could not load media.");
			}
		}
	};

	xhr.open("GET", window.location.origin + '/getmedia?' + formQuery(dts), true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
	xhr.send(null);
}

function cancelMediaDialog()
{
	let dialog = document.getElementById("media-dialog");
	document.body.removeChild(dialog);
	_dialogClose();
}
