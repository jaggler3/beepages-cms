var quillObj, _pageURL, _vaction, catdata;
var toolbarOptions = [
	['bold', 'italic', 'underline', 'strike'],			// toggled buttons
	['blockquote', 'code-block'],

	[{ 'header': 1 }, { 'header': 2 }],					// custom button values
	[{ 'list': 'ordered'}, { 'list': 'bullet' }],
	[{ 'script': 'sub'}, { 'script': 'super' }],		// superscript/subscript
	[{ 'indent': '-1'}, { 'indent': '+1' }],			// outdent/indent
	[{ 'direction': 'rtl' }],							// text direction

	[{ 'size': ['small', false, 'large', 'huge'] }],	// custom dropdown
	[{ 'header': [1, 2, 3, 4, 5, 6, false] }],
	[ 'link', 'image', 'video'],						// add's image support
	[{ 'color': [] }, { 'background': [] }],			// dropdown with defaults from theme
	[{ 'font': [] }],
	[{ 'align': [] }],

	['clean']											// remove formatting button
];

function showEditor(pageURL)
{
	let pageIndex = -1;
	for(let i = 0; i < PAGE_LIST.length; i++)
	{
		if(PAGE_LIST[i][2] == pageURL)
		{
			pageIndex = i;
		}
	}

	let pageTitleField = document.getElementById('pageTitleLabel');
	pageTitleField.innerHTML += '<u>' + decodeURIComponent(PAGE_LIST[pageIndex][1]) + '</u>';

	let cv = PAGE_LIST[pageIndex][6];
	let vb = document.getElementById("pageVisEdit");
	if(cv == "d")
	{
		_vaction = "p";
		vb.innerHTML = "Publish";
	} else if(cv == "h")
	{
		_vaction = "p";
		vb.innerHTML = "Make Public";
	} else
	{
		_vaction = "h";
		vb.innerHTML = "Archive";
	}

	let editorView = document.getElementById('editor');
	_pageURL = PAGE_LIST[pageIndex][2];
	getFileContents('./admin/data/' + _pageURL + '.html?token=' + window.bee.sessionToken, function(data) {
		getFileContents('./getcategories?token=' + window.bee.sessionToken, function(data2)
	{
		catdata = eval(data2);

		editorView.innerHTML = data;

		quillObj = new Quill(editorView, {
			modules: {
				toolbar: {
					container: '#toolbar-container',
					handlers: {
						image: imageHandler
					}
				},
			},
			theme: 'snow'
		});

		let catlistDom = document.getElementById("catlist");
		catlistDom.setAttribute("size", catdata.length + 1);
		for(let e = 0; e < catdata.length; e++)
		{
			if(PAGE_LIST[pageIndex][5] == catdata[e]) { continue; }
			let nn = document.createElement("option");
			nn.setAttribute("value", catdata[e]);
			catlistDom.appendChild(nn);
		}
		document.getElementById("pageCategory").value = PAGE_LIST[pageIndex][5];
	})});
}

function imageHandler(value)
{
	let range = this.quill.getSelection();
	showMediaDialog(function(value)
	{
		quillObj.insertEmbed(range.index, "image", value, Quill.sources.USER);
	});
}

function pageVisibility()
{
	let xhr = new XMLHttpRequest();
	xhr.open("POST", window.location.origin + '/visibility', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		content:
		{
			url: _pageURL,
			new: _vaction
		}
	}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function () {
		let vb = document.getElementById("pageVisEdit");
		if(_vaction == "d")
		{
			vb.innerHTML = "Publish";
		} else if(_vaction == "h")
		{
			vb.innerHTML = "Make Public";
		} else if(_vaction == "p")
		{
			vb.innerHTML = "Archive";
		}

		alert('Visiblity changed.');
	};
}

function pageCategory()
{
	if(document.getElementById("pageCategory").value == "none") { return; }
	let xhr = new XMLHttpRequest();
	xhr.open("POST", window.location.origin + '/setcategory', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		content:
		{
			url: _pageURL,
			category: document.getElementById("pageCategory").value
		}
	}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function () {
		alert('Category changed.');
	};
}

function savePage()
{
	let pageData = quillObj.container.firstChild.innerHTML;

	let xhr = new XMLHttpRequest();
	xhr.open("POST", window.location.origin + '/update_content', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		content:
		{
			url: 'data/' + _pageURL + '.html',
			data: pageData
		}
	}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function () {
		alert('Saved.');
	};
}
