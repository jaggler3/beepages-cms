function installTheme(themeName)
{
	console.log("installing '" + themeName + "'");
	_dialogOpen();
	//TODO: cache dialog data
	getFileContents('admin/dialogs/install?token=' + window.bee.sessionToken, function(data)
	{
		let dd = document.createElement('div');
		dd.innerHTML = data;
		let dialog = dd.firstChild;

		let children = dialog.childNodes;
		for(let i = 0; i < children.length; i++)
		{
			let child = children[i];
			if(child.id == "dialog-title")
			{
				child.innerHTML = "Installing '" + themeName + "'";
			}
		}

		document.body.appendChild(dialog);

		startThemeDownload(themeName);
	});
}

function startThemeDownload(themeName)
{
	let xhr = new XMLHttpRequest();
	xhr.open("POST",  window.location.origin + '/installtheme', true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	let dts = {
		sender:
		{
			token: window.bee.sessionToken
		},
		themeName: themeName
	}

	xhr.send(JSON.stringify(dts));

	xhr.onloadend = function ()
	{
		let dialog = document.getElementById("install-dialog");
		document.body.removeChild(dialog);
		_dialogClose();
	};
}

function cancelCreateDialog()
{
	let dialog = document.getElementById("new-dialog");
	document.body.removeChild(dialog);
	_dialogClose();
}
