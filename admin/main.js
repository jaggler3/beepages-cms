[[./public/getfile.js]]

var em = {
	vc: undefined,
}

var initView = "{{initView}}";

window.bee = {
	currentView: undefined,
	sessionToken: undefined,
}

var WIN_LOAD_QUEUE = [];

window.onload = function()
{
	window.bee.sessionToken = getQueryStringValue("token");
	document.getElementById("title").setAttribute("href", window.location.origin + "/admin");
	document.getElementById("goto-site-button").setAttribute("onclick", "window.open(window.location.origin, '_blank')");
	em.vc = document.getElementById("view-container");
	let title = "BeePages";
	if(initView == "posts" || initView == "pages" || initView == "analytics" || initView == "theme")
	{
		title += " - " + (initView.charAt(0) + "").toUpperCase() + initView.substring(1);
		selectView(document.getElementById("selector-" + initView), initView);
	}
	document.title = title;

	WIN_LOAD_QUEUE.map(function(x) { (x || function(){})(); })
}

function selectView(select, viewName, input)
{
	if(select !== undefined)
	{
		if(window.bee.lastSelected !== undefined)
		{
			window.bee.lastSelected.className = window.bee.lastSelected.className.replace(" top-selected", "");
		}
		select.className += " top-selected";
		window.bee.lastSelected = select;
	} else
	{
		let _sb = document.getElementById("selector-" + viewName);
		if(_sb !== undefined)
		{
			(_sb = window.bee.lastSelected).className += " top-selected"
		}
	}
	window.bee.currentView = viewName;
	getFileContents('admin/views/' + viewName + "?token=" + window.bee.sessionToken, function(data) {
		em.vc.style.justifyContent = "flex-start";
		em.vc.innerHTML = data;

		if(viewName == "posts")
		{
			showPosts();
		} else if(viewName == 'pages')
		{
			showPages();
		} else if(viewName == 'editor')
		{
			showEditor(input);
		} else if(viewName == 'theme')
		{
			showThemeSelect();
		} else if(viewName == 'analytics')
		{
			showAnalytics();
		}

		if(history.pushState && (viewName == "posts" || viewName == "pages" || viewName == "analytics" || viewName == "theme"))
		{
			history.pushState({}, null, location.origin + "/admin/selected/" + viewName + "?token=" + window.bee.sessionToken);
		}
	});
}

var PAGE_LIST = [];
var PAGE_LIST_LANDING = 0;

function getQueryStringValue(key)
{
	return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

function getPageList(callback)
{
	getFileContents('pagelist?token=' + window.bee.sessionToken, function(data) {
		callback((function (){ let _rv = undefined; eval("_rv = " + data); return _rv; })());
	});
}

function updatePageList(callback)
{
	getPageList(function(list)
	{
		PAGE_LIST = list.LIST;
		PAGE_LIST_LANDING = list.LANDING;
		callback();
	});
}

function formQuery(d)
{
	let q = "";
	for(let i = 0; i < d.length; i++) { q += "&" + d[i][0] + "=" + d[i][1]; }
	if(d.length > 0) { q = q.substring(1); }
	return q;
}

function _dialogOpen()
{
	let btns = document.getElementsByTagName("button");
	for(let i = 0; i < btns.length; i++)
	{
		btns[i].disabled = true;
	}
}

function _dialogClose()
{
	let btns = document.getElementsByTagName("button");
	for(let i = 0; i < btns.length; i++)
	{
		btns[i].disabled = false;
	}
}
