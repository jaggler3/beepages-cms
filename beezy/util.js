"use strict";

var fs = require('fs');
var http = require('http');
var https = require('https');
var url = require('url');
var path = require('path');

module.exports = {
	_MS_PER_DAY: 1000 * 60 * 60 * 24,
	loadFile: function(srcpath, errcb)
	{
		let fileContents = "";
		try
		{
			fileContents = fs.readFileSync(srcpath, 'utf8');
		} catch (err)
		{
			if(errcb)
				errcb(err);
		}
		return fileContents;
	},
	loadFileAsync: function(srcpath, callback, errcb)
	{
		fs.readFile(srcpath, 'utf8', function (err, data)
		{
			if (err && errcb)
			{
				errcb(err);
			}
			callback(data.toString());
		});
	},
	saveFile: function(srcpath, file)
	{
		fs = require('fs');
		fs.writeFile(srcpath, file, function (err)
		{
			if (err)
				throw err;
		});
	},
	deleteFile: function(srcpath)
	{
		fs.unlink(srcpath, function(err){ console.log(err || ""); })
	},
	mergeData: function(destination, source)
	{
		for (var property in source)
		{
			if (source.hasOwnProperty(property))
			{
				destination[property] = source[property];
			}
		}
		return destination;
	},
	dateAdd: function(date, type, amount){
		var y = date.getFullYear(), m = date.getMonth(), d = date.getDate();
		if(type === 'y') { y += amount; } else if(type === 'm') { m += amount; } else if(type === 'd') { d += amount; }
		return new Date(y, m, d);
	},
	difference_days: function(a, b)
	{
	  	var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
		var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

	  	return Math.floor((utc2 - utc1) / Util._MS_PER_DAY);
	},
	download: function(srcpath, dest, callback)
	{
		var file = fs.createWriteStream(dest);
		var request = (url.parse(srcpath).protocol == "http" ? http : https).get(srcpath, function(response)
		{
			response.pipe(file);
			file.on('finish', function()
			{
				file.close(callback);
			});
		});
	},
	get: function(srcpath, callback)
	{
		(url.parse(srcpath).protocol == "http" ? http : https).get(srcpath, function(response)
		{
			response.setEncoding('utf-8');
			response.on('data', function(data)
			{
				callback(data.toString());
			});
		});
	},
	ensureDir: function(dest, location)
	{
		if(dest.indexOf("/") != -1)
		{
			let wp = dest.startsWith("/") ? dest.substring(1) : dest;
			if(wp.indexOf("/") != -1)
			{
				let coll = location;
				let dirs = wp.split("/");
				for(let i = 0; i < dirs.length - 1; i++)
				{
					coll += "/" + dirs[i];
					if (!fs.existsSync(coll))
					{
					    fs.mkdirSync(coll);
					}
				}
			}
		}
	},
	listDir: function(dir_path, callback)
	{
		fs.readdir(dir_path, (err, files) => {
			callback(files);
		});
	},
	deleteDir: function(dir_path)
	{
		let tu = this;
	    if (fs.existsSync(dir_path))
	    {
	        fs.readdirSync(dir_path).forEach(function(entry)
	        {
	            var entry_path = path.join(dir_path, entry);
	            if (fs.lstatSync(entry_path).isDirectory())
	            {
	                tu.deleteDir(entry_path);
	            } else {
	                fs.unlinkSync(entry_path);
	            }
	        });
	        fs.rmdirSync(dir_path);
	    }
	},
}
