const Util = require('./util.js');
const Database = require('./db.js');
const DataTable = Database.DataTable;

var Analytics = {
	pageList: require('./../private/pagelist.js'),
	sessionTimeout: 120, //seconds
	sessions: [],
	register: function(url, userData)
	{
		let sub = url;
		//remove slashes
		if(url != "/")
		{
			if(url.startsWith("/")) { sub = sub.substring(1); }
			if(url.endsWith("/")) { sub = sub.substring(0, sub.length - 1); }
		} else { sub = "home"; }

		let out = Analytics.validate(sub, userData);
		if(out.valid && Database.getLoaded())
		{
			let _idx = Database.find_any("pageview", [sub]);
			if(_idx == -1)
			{
				Database.post("pageview", [sub, 1]);
			} else
			{
				Database.tables["pageview"].data[_idx][1]++;
			}

			let aidx = Database.find_any("best", ["all"]);
			if(aidx == -1)
			{
				Database.post("best", ["all", 1]);
			} else
			{
				Database.tables["best"].data[aidx][1]++;
			}

			if(Analytics.find_session(userData.localAddress) == -1)
			{
				Analytics.sessions.push(new SiteSession(userData.localAddress));
			} else
			{
				var sess = Analytics.sessions[Analytics.find_session(userData.localAddress)];
				sess.pageCount++;
				sess.lastTime = new Date().getTime();
			}
		}
	},
	find_session: function(address)
	{
		for(let i = 0; i < Analytics.sessions.length; i++)
		{
			if(Analytics.sessions[i].address == address)
			{
				return i;
			}
		}
		return -1;
	},
	clean: function()
	{
		let _d = new Date();
		let dt = _d.getTime();
		for(let i = 0; i < Analytics.sessions.length; i++)
		{
			if(dt + (Analytics.sessionTimeout * 1000) > Analytics.sessions[i].lastTime)
			{
				Analytics.sessions.splice(i, 1);
			}
		}

		// TODO: account for user changing time backwards, daylight savings, etc
		while(Database.tables["old-week"].entryCount >= 8)
		{
			// TODO: save old data in db somewhere
			if(Database.tables["old-week"].entryCount == 8)
			{
				Database.clear_table("pageview");
			}
			Database.delete_entry("old-week", 0);
		}

		let cdt = _d.getFullYear() + "-" + (_d.getMonth() + 1) + "-" + _d.getDate();
		let _idx = Database.find_any("old-week", [cdt]);
		if(_idx == -1)
		{
			Database.post("old-week", cdt);
		}
		else
		{
			Database.set("old-week", _idx, [cdt, Analytics.dataf.today_views()]);
		}
	},
	validate: function(url, userData)
	{
		let res = { valid: Analytics.matchUrl(url) };
		return res;
	},
	matchUrl: function(url)
	{
		let bl = [
			"home",
			"glossary",
		]
		if(bl.indexOf(url) != -1)
		{
			return true;
		}

		let sub = url;

		for(let i = 0; i < Analytics.pageList.LIST.length; i++)
		{
			let entry = Analytics.pageList.LIST[i];
			if(sub == entry[2] ||
				sub == "posts/" + entry[2]
			)
			{
				return true;
			}

			//check if within a category?
		}

		return false;
	},
	updateData: function()
	{
		Analytics.pageList = require('./../private/pagelist.js');
	},
	saveData: function()
	{
		Database.save();
	},
	getData: function()
	{
		let res = {
			all_views: Analytics.dataf.get_all(),
			today_views: Analytics.dataf.today_views(),
			week_views: Analytics.dataf.get_week(),
			week_dates: Analytics.dataf.get_week_dates(),
			week_data: Analytics.dataf.get_week_data(),
			today_pages: Analytics.dataf.get_today_pages(),
			today_data: Analytics.dataf.get_today_data(),
		};
		return res;
	},
	dataf: {
		today_views: function()
		{
			return Database.table_reduce_num("pageview", 1, function(total, val)
			{
				return total + val;
			}, 0);
		},
		get_week: function()
		{
			return Database.table_reduce_num("old-week", 1, function(total, val)
			{
				return total + val;
			}, 0);
		},
		get_week_dates: function()
		{
			return Database.list_column("old-week", 0);
		},
		get_week_data: function()
		{
			return Database.list_column("old-week", 1);
		},
		get_today_pages: function()
		{
			return Database.list_column("pageview", 0);
		},
		get_today_data: function()
		{
			return Database.list_column("pageview", 1);
		},
		get_all: function()
		{
			return (Database.find_any_entry("best", ["all"]) || [undefined, 0])[1] || Analytics.dataf.get_week();
		}
	},
};

function SiteSession(address)
{
	this.address = address;
	this.pageCount = 0;
	this.lastTime = new Date().getTime();
}

module.exports.SiteSession = SiteSession;
module.exports = Analytics;
