const Util = require('./util.js');
const fs = require('fs');

var Database = {
	tableCount: 0,
	tableNames: [],
	tables: {},
	_loaded: false,
	getLoaded: function()
	{
		return Database._loaded;
	},
	initialize: function()
	{
		if(fs.existsSync('./.bpdb'))
		{
			Util.loadFileAsync('./.bpdb', function(data)
			{
				Database.parseData(data);
				Database._loaded = true;
			});
		} else
		{
			let ow_table = Database.add_table("old-week");
			let pv_table = Database.add_table("pageview");
			let b_table = Database.add_table("best");
			Database.post("best", ["day", 0]);
			Database.post("best", ["week", 0]);
			Database.post("best", ["all", 0]);
			Database._loaded = true;
		}
	},
	parseData: function(data)
	{
		let lines = data.split("\n");
		let currentTable = undefined;
		for(let i = 0; i < lines.length; i++)
		{
			let line = lines[i].trim();
			if(line.startsWith("#"))
			{
				currentTable = Database.add_table(line.substring(1));
			} else if(line.startsWith("END"))
			{
				Database.tables[currentTable.name] = currentTable;
				currentTable = undefined;
			} else if(line.length > 0)
			{
				currentTable.data[currentTable.entryCount++] = line.split(";");
			}
		}
	},
	add_table: function(tableName)
	{
		let nt = new DataTable(tableName);
		Database.tables[tableName] = nt;
		Database.tableNames.push(tableName);
		Database.tableCount++;
		return nt;
	},
	query_all: function(tableName)
	{
		return Database.tables[tableName].data;
	},
	find_any_entry: function(tableName, conditions)
	{
		return Database.tables[tableName].data[Database.find_any(tableName, conditions)];
	},
	post: function(tableName, data)
	{
		Database.tables[tableName].data[Database.tables[tableName].entryCount++] = data;
	},
	set: function(tableName, index, data)
	{
		Database.tables[tableName].data[index] = data;
	},
	get: function(tableName, index)
	{
		return Database.tables[tableName].data[index];
	},
	index_exists: function(tableName, index)
	{
		return Database.tables[tableName][index] !== undefined;
	},
	delete_entry: function(tableName, index)
	{
		let table = Database.tables[tableName];
		delete table.data[index];
		for(let i = 0; i < table.entryCount; i++)
		{
			if(i > index)
			{
				let tmp = table.data[i];
				delete table.data[i];
				table.data[i - 1] = tmp;
			}
		}
		table.entryCount--;
	},
	table_reduce: function(tableName, column, rdc, init)
	{
		let total = init;
		for(let i = 0; i < Database.tables[tableName].entryCount; i++)
		{
			total = rdc(total, Database.tables[tableName].data[i][column]);
		}
		return total;
	},
	table_reduce_num: function(tableName, column, rdc, init)
	{
		let total = init;
		for(let i = 0; i < Database.tables[tableName].entryCount; i++)
		{
			total = rdc(total, Number(Database.tables[tableName].data[i][column]));
		}
		return total;
	},
	find_strict: function(tableName, conditions)
	{
		let table = Database.tables[tableName];
		for(let i = 0; i < table.entryCount; i++)
		{
			let t = true;
			for(let j = 0; j < conditions.length; j++)
			{
				if(table.data[i].indexOf(conditions[j]) == -1)
				{
					t = false;
				}
			}
			if(t)
			{
				return i;
			}
		}

		return -1;
	},
	find_any: function(tableName, conditions)
	{
		let table = Database.tables[tableName];
		for(let i = 0; i < table.entryCount; i++)
		{
			for(let j = 0; j < conditions.length; j++)
			{
				if(table.data[i].indexOf(conditions[j]) != -1)
				{
					return i;
				}
			}
		}

		return -1;
	},
	save: function()
	{
		let res = "";
		let nl = "\n";
		for(let i = 0; i < Database.tableCount; i++)
		{
			let table = Database.tables[Database.tableNames[i]];
			res += "#" + table.name + nl;
			if(table.entryCount > 0)
			{
				for(let j = 0; j < table.entryCount; j++)
				{
					res += table.data[j] !== undefined ? table.data[j].join(";") + nl : "";
				}
			}
			res += "END" + nl;
		}

		Util.saveFile('./.bpdb', res);
	},
	clear_table: function(tableName)
	{
		Database.tables[tableName].entryCount = 0;
		Database.tables[tableName].data = {};
	},
	delete_table: function(tableName)
	{
		Database.tableCount--;
		Database.tableNames.splice(Database.tableNames.indexOf(tableName), 1);
		delete Database.tables[tableName];
	},
	list_column: function(tableName, column)
	{
		let res = [];
		let table = Database.tables[tableName];
		for(let i = 0; i < table.entryCount; i++)
		{
			res.push(table.data[i][column]);
		}
		return res;
	},
};

function DataTable(name)
{
	this.name = name;
	this.data = {};
	this.entryCount = 0;
}

module.exports = Database;
module.exports.DataTable = DataTable;
