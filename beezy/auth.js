var crypto = require('crypto'),
    algorithm = 'aes-256-ctr';

const Util = require('./util.js');

var Auth = {
	tokens: [],
	sessionTimes: [],
	poll: function(timeout)
	{
		for(let i = Auth.sessionTimes.length; i > -1; i--)
		{
			if(Date.now() - Auth.sessionTimes[i] > timeout * 60 * 1000)
			{
				Auth.sessionTimes.splice(i, 1);
				Auth.tokens.splice(i, 1);
			}
		}
	},
    text_encrypt: function(str)
	{
		var cipher = crypto.createCipher(algorithm, str);
		var crypted = cipher.update(str, 'utf8', 'hex');
		crypted += cipher.final('hex');
		return crypted;
    },
	text_recieve: function(password, key)
	{
		var decipher = crypto.createDecipher(algorithm, password);
		var dec = decipher.update(key, 'hex', 'utf8');
		dec += decipher.final('utf8');
		return dec;
	},
	createToken: function()
	{
		let nt = Auth._genToken();
		Auth.tokens.push(nt);
		Auth.sessionTimes.push(Date.now());
		return nt;
	},
	hasToken: function(token)
	{
		let i = Auth.tokens.indexOf(token);
		if(token === undefined || i == -1)
		{
			return false;
		} else
		{
			Auth.sessionTimes[i] = Date.now();
			return true;
		}
	},
	_genToken: function()
	{
		return crypto.randomBytes(20).toString('hex');
	},
	check: function(username, password)
	{
		let data = Util.loadFile("./.users");
		let lines = data.split("\n");
		for(let i = 0; i < lines.length; i++)
		{
			let lineData = lines[i].split(" ");
			if(lineData[1] == username && Auth.text_recieve(password, lineData[2]) == password)
			{
				return i;
			}
		}
		return -1;
	},
};

module.exports = Auth;
