function Generator()
{
	this.name = "[gen]";
	this.inputData = {};
	this.generate = function(){};
	this.children = [];
	this.generateChildren = function()
	{
		var res = "";
		for(var i = 0; i < this.children.length; i++)
		{
			res += this.children[i].generate();
		}
		return res;
	}
}

module.exports = Generator;
