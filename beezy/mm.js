const Util = require('./util.js');

var MediaManager = {
     getMediaList: function(callback)
     {
          Util.listDir("./uploads", callback);
     },
};

module.exports = MediaManager;
