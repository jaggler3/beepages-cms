"use strict";

var express 		= require('express'), app = express();
var bodyParser 		= require('body-parser');
var fs 				= require('fs');
var multer			= require('multer')
var pathlib			= require('path');
var urllib			= require('url');

const Generator 	= require('./beezy/generator.js');
const Util 			= require('./beezy/util.js');
const Analytics 	= require('./beezy/analytics.js');
const Database 		= require('./beezy/db.js');
const MediaManager	= require('./beezy/mm.js');
const Auth			= require('./beezy/auth.js');
const themeLocation = "https://serene-petrified-forest-51185.herokuapp.com/";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.enable('trust proxy');

var firstAdmin = false;

var storage = multer.diskStorage({
	destination: function (req, file, cb)
	{
		cb(null, './uploads')
	},
	filename: function (req, file, cb)
	{
		cb(null, file.originalname);
	}
})

var upload = multer({ storage: storage })

var displayParams = {};

var siteData = {names: [], keys: [], data: {}};

var Beezy = {
	generators: {},
	Parse: function(data, params, path)
	{
		var res = data + "";

		res = res.replace(/\[\[[\S]+\]\]/g, function rep(x)
		{
			let name = x.substring(2, x.length - 2);
			if(name.startsWith("::"))
			{
				let ind = Math.max(path.lastIndexOf("/"), path.lastIndexOf("\\"));
				name = path.substring(0, ind + 1) + name.substring(2);
			}
			return Beezy.Parse(Beezy.GetData(Beezy.ApplyParams(
				Util.loadFile(name), params)
			), params, name);
		});

		return Beezy.GetData(Beezy.ApplyParams(res, params));
	},
	GetData: function(data)
	{
		var res = data + "";

		res = res.replace(/\`[\S]+\`/g, function rep(x)
		{
			let name = x.substring(1, x.length - 1).trim();
			let _d = Util.loadFile('./data/' + name);
			return _d.length > 0 ? _d : Util.loadFile('./theme/current/notfound.html');
		});

		return res;
	},
	ApplyParams: function(data, params)
	{
		var res = data + "";
		res = res.replace(/{{(.|\n)*?}}/g, function rep(x)
		{
			let name = x.substring(2, x.length - 2);
			if(name.startsWith("+"))
			{
				let si = name.indexOf(" ");
				displayParams[name.substring(1, si)] = name.substring(si + 1);
				return "";
			}
			return params ? (params[name] || "null") : "null";
		});

		return res;
	},
	pageList: function()
	{
		return require('./private/pagelist.js');
	},
	savePageList: function(newList)
	{
		Util.saveFile('./private/pagelist.js',
		'var PAGE_LIST = '
		+ JSON.stringify(newList, null, '\t')
		+ ';\n'
		+ 'var PAGE_LIST_LANDING = 0; var module = module || {}; module.exports = {LIST: PAGE_LIST, LANDING: PAGE_LIST_LANDING};');
	},
	saveSiteData: function()
	{
		let res = "";
		for(let i = 0; i < siteData.names.length; i++)
		{
			res += siteData.names[i] + "=" + siteData.keys[i] + "=" + siteData.data[siteData.keys[i]] + "\n";
			displayParams[siteData.keys[i]]= siteData.data[siteData.keys[i]];
		}
		Util.saveFile('./.sitedata', res);
	},
};

function LoadGenerators()
{
	var lines = Util.loadFile('./theme/current/theme.bzt').split('\n');
	for(let i = 0; i < lines.length; i++)
	{
		if(lines[i].charAt(0) == '-' || lines[i].length < 1) { continue; }
		Beezy.generators[lines[i].trim()] = CreateGenerator(lines[i].trim());
	}
}

function CreateGenerator(name)
{
	var generator = new Generator();
	generator.name = name;

	generator.inputData = displayParams;

	generator.generate = function()
	{
		let path = './theme/current/' + name + '.html';
		return Beezy.Parse(Beezy.GetData(Beezy.ApplyParams(
			Util.loadFile(path), generator.inputData
		)), generator.inputData, path);
	};

	return generator;
}

function LoadSiteData()
{
	let lines = Util.loadFile('./.sitedata').split('\n');
	for(let i = 0; i < lines.length; i++)
	{
		if(lines[i].length == 0) { continue; }
		let ld = lines[i].split("=");
		if(ld.length != 3) { continue; }
		let key = ld[1].trim();
		siteData.names.push(ld[0].trim());
		siteData.keys.push(key);
		displayParams[key] = ld[2].trim();
		siteData.data[key] = displayParams[key];
	}
}

function Initialize()
{
	LoadSiteData();
	LoadGenerators();
	Database.initialize();

	app.use(function(req, res, next)
	{
		if(!req.path.startsWith("/admin")) { next(); return; }

		if(Auth.hasToken(req.query.token) || req.path.startsWith("/admin/assets"))
		{
			next();
		} else
		{
			if(req.path.startsWith("/admin/login"))
			{
				next();
			} else
			{
				res.redirect("/admin/login");
			}
		}
	});

	//ADMIN ASSETS
	app.get('/admin/assets/:assetName', function(req, res)
	{
		let path = './admin/assets/' + req.params.assetName;
		res.sendFile(path, {root : __dirname});
	});

	//NEW SITE?
	app.get('/admin/login/new', function(req, res)
	{
		let userFile = Util.loadFile("./.users");
		res.send((userFile.length == 0) + "");
	});

	//CREATE ADMIN
	app.post('/admin/login/create', function(req, res)
	{
		Util.saveFile("./.users", "0 " + req.body.username + " " + Auth.text_encrypt(req.body.password));
		firstAdmin = true;
		res.send("FINISHED");
	});

	//LOGIN PAGE
	app.get('/admin/login', function(req, res)
	{
		let path = './admin/login.html';
		res.send(Beezy.Parse(Util.loadFile(path), undefined, path));
	});

	//LOGIN
	app.get('/admin/login/try', function(req, res)
	{
		if(firstAdmin)
		{
			res.send(Auth.createToken());
			firstAdmin = false;
		} else
		{
			if(Auth.check(req.query.username, req.query.password) != -1)
			{
				res.send(Auth.createToken());
			} else
			{
				res.send("unsuccessful");
			}
		}
	});

	//FAVICON
	app.use('/favicon.ico', function (req, res)
	{
		res.sendFile('/favicon.ico' , {root : __dirname});
	});

	//SITE UTIL DATA
	app.use('/public', express.static('public'));

	//ADMIN TOOLS
	app.get('/admin', function (req, res)
	{
		let path = './admin/main.html';
		res.send(Beezy.Parse(Util.loadFile(path), undefined, path));
	});
	app.get('/admin/views/:viewName', function (req, res)
	{
		let path = './admin/views/' + req.params.viewName + '.html';
		let fd = Util.loadFile(path);
		res.send(fd != "" ? Beezy.Parse(fd, undefined, path) : "");
	});
	app.get('/admin/selected/:viewName', function (req, res)
	{
		let path = './admin/main.html';
		let fd = Util.loadFile(path);
		res.send(fd != "" ? Beezy.Parse(fd, {initView: req.params.viewName}, path) : "");
	})
	app.get('/admin/dialogs/:dialogName', function (req, res)
	{
		let path = './admin/views/dialogs/' + req.params.dialogName + '.html';
		let fd = Util.loadFile(path);
		res.send(fd != "" ? Beezy.Parse(fd, undefined, path) : "");
	});
	app.get('/admin/data/:dataName', function (req, res)
	{
		let path = './data/' + req.params.dataName;
		let fd = Util.loadFile(path);
		res.send(fd != "" ? Beezy.Parse(fd, undefined, path) : fd);
	});

	app.post('/update_content', function (req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			let past = Beezy.pageList().LIST;
			for(let i = 0; i < past.length; i++)
			{
				if("data/" + past[i][2] + ".html" == pd.content.url)
				{
					past[i][3] = new Date().toLocaleDateString("en-US", {month: 'long', day: 'numeric', year: 'numeric'});
					past[i][4] = pd.content.data.substring(0, 75);
					Beezy.savePageList(past);
					Util.saveFile(pd.content.url, pd.content.data);
					break;
				}
			}
		}
		res.send('FINISHED');
	});

	app.post('/rename', function (req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			fs.rename('data/' + pd.content.previous[1] + '.html', 'data/' + pd.content.new[1] + '.html', function(err){
				if (err) console.log(err);
			});
			let past = Beezy.pageList().LIST;
			for(let i = 0; i < past.length; i++)
			{
				if(past[i][1] == pd.content.previous[0])
				{
					past[i][1] = pd.content.new[0];
					past[i][2] = pd.content.new[1];
					Beezy.savePageList(past);
					break;
				}
			}
		}
		res.send('FINISHED');
	});

	app.post('/visibility', function (req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			let past = Beezy.pageList().LIST;
			for(let i = 0; i < past.length; i++)
			{
				if(past[i][2] == pd.content.url)
				{
					past[i][6] = pd.content.new;
					Beezy.savePageList(past);
					break;
				}
			}
		}
		res.send('FINISHED');
	});

	app.post('/create', function (req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			Util.saveFile('./data/' + pd.content.new[1] + '.html', "");
			let past = Beezy.pageList().LIST;
			past.push([
					pd.content.type,
					pd.content.new[0],
					pd.content.new[1],
					new Date().toLocaleDateString("en-US", {month: 'long', day: 'numeric', year: 'numeric'}),
					"",
					"",
					"d"
				 ]);
			Beezy.savePageList(past);
		}
		res.send('FINISHED');
	});

	app.post('/setcategory', function (req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			let past = Beezy.pageList().LIST;
			for(let i = 0; i < past.length; i++)
			{
				if(past[i][2] == pd.content.url)
				{
					past[i][5] = pd.content.category;
					Beezy.savePageList(past);
					break;
				}
			}
		}
		res.send('FINISHED');
	});
	app.get('/getcategories', function (req, res)
	{
		if(Auth.hasToken(req.query.token))
		{
			let out = [];
			let list = Beezy.pageList().LIST;
			for(let i = 0; i < list.length; i++)
			{
				if(list[i][5] != "" && out.indexOf(list[i][5]) == -1)
				{
					out.push(list[i][5]);
				}
			}

			res.send(JSON.stringify(out));
		}
	});

	app.post('/installtheme', function (req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			var tp = './theme/current/';
			Util.deleteDir(tp);

			if (!fs.existsSync(tp))
			{
			    fs.mkdirSync(tp);
			}

			Util.get(themeLocation + "listfiles?theme=" + pd.themeName, function(jsonData)
			{
				let list = JSON.parse(jsonData);
				for(let i = 0; i < list.files.length; i++)
				{
					//TODO: make async, show steps in client
					Util.ensureDir(list.files[i], tp);
					Util.download(themeLocation + "files/" + pd.themeName + "/" + list.files[i], tp + "/" + list.files[i], () => {
						if(i == list.files.length - 1)
						{
							res.send("FINISHED");
						}
					});
				}
			});
		}
	});

	app.post('/delete', function (req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			let past = Beezy.pageList().LIST;
			for(let i = 0; i < past.length; i++)
			{
				if(past[i][1] == pd.content.name)
				{
					Util.deleteFile('./data/' + past[i][2] + '.html');
					past.splice(i, 1);
					Beezy.savePageList(past);
					break;
				}
			}
		}
		res.send('FINISHED');
	});

	app.get('/pagelist', function(req, res)
	{
		let list = Beezy.pageList();
		if(Auth.hasToken(req.query.token))
		{
			res.send(list);
		} else
		{
			let nl = [];
			let _hidden = [
				"d",	//draft
				"h",	//hidden/archived
			]
			for(let i = 0; i < list.LIST.length; i++)
			{
				if(_hidden.indexOf(list.LIST[i][6]) == -1)
				{
					nl.push(list.LIST[i]);
				}
			}
			let nlo = {
				LIST: nl,
				LANDING: list.LANDING
			};
			res.send(nlo);
		}
	});

	app.get('/getsettings', function(req, res)
	{
		if(Auth.hasToken(req.query.token))
		{
			res.send(siteData);
		}
	});

	app.post('/setsettings', function(req, res)
	{
		let pd = req.body;
		if(Auth.hasToken(pd.sender.token))
		{
			for(let i = 0; i < pd.content.changes.length; i++)
			{
				siteData.data[pd.content.changes[i].key] = pd.content.changes[i].value;
				Beezy.saveSiteData();
			}
		}
		res.send('FINISHED');
	});

	app.get('/uploads/:fileName', function(req, res)
	{
		res.sendFile(__dirname + '/uploads/' + req.params.fileName);
	});

	app.get('/analytics_data', function(req, res)
	{
		if(Auth.hasToken(req.query.token))
		{
			res.send(Analytics.getData());
		} else
		{
			res.send(null);
		}
	});

	app.post('/upload', upload.single("media"), function(req, res)
	{
        res.end("File uploaded");
	});

	app.get('/getmedia', function(req, res)
	{
		if(Auth.hasToken(req.query.token))
		{
			MediaManager.getMediaList(function(list)
			{
				res.send(list);
			});
		} else
		{
			res.send(null);
		}
	});

	//THE SITE
	var loaderData = Util.loadFile('./theme/loader.txt').split("\n");
	for(let i = 0; i < loaderData.length; i++)
	{
		var lineData = loaderData[i].split("@");
		if(loaderData[i].length <= 1) { continue; }

		let access = lineData[0].trim();
		let genName = lineData[1].trim();

		app.get(access, function(req, res)
		{
			let cg = Beezy.generators[genName];
			let _pageName = "";
			let pl = Beezy.pageList().LIST;
			for(let j = 0; j < pl.length; j++)
			{
				if(req.params.postID === undefined && req.params.pageID === undefined) { break; }
				if(pl[j][2] == req.params.postID) { _pageName = pl[j][1]; break; }
				if(pl[j][2] == req.params.pageID) { _pageName = pl[j][1]; break; }
			}

			let _ip = req.ip || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
			Analytics.register(req.url, {
				localAddress: _ip.substring(0, 7) == "::ffff:" ? _ip.substring(7) : _ip
			});
			cg.inputData = Util.mergeData(displayParams, req.params || {});
			cg.inputData.PATH_ORIGIN = req.protocol + '://' + req.get('host');
			cg.inputData.pageName = _pageName;
			res.send(cg.generate());
		});
	}

	//ALWAYS USE AS LAST ROUTE
	app.get('*', function(req, res)
	{
		res.status(404).send(Util.loadFile('./theme/current/notfound.html'));
	});

	//ANALYTICS
	setInterval(function()
	{
		Analytics.updateData();
		Analytics.clean();
		Auth.poll(Number(siteData["A_TIMEOUT"]));
	}, 1000); // every second
	setInterval(function()
	{
		Analytics.saveData();
	}, 10000); //updates database file every 10 seconds
}

Initialize();

app.listen(8080, "127.0.0.1");
